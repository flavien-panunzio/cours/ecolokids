﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class cinematique : MonoBehaviour
{

    public GameObject video1;
    public GameObject video2;
    public GameObject Sonvideo2;
    public GameObject Canvas;


    // Start is called before the first frame update
    void Start()
    {
        video1.GetComponent<VideoPlayer>().loopPointReached += CheckVideo1;
        video2.GetComponent<VideoPlayer>().loopPointReached += CheckVideo2;
        Canvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CheckVideo1(UnityEngine.Video.VideoPlayer vp)
    {
        video2.GetComponent<VideoPlayer>().Play();
        Sonvideo2.GetComponent<AudioSource>().Play();
        Canvas.SetActive(true);
    }

    void CheckVideo2(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene("Menu");
    }

    public void Skip()
    {
        SceneManager.LoadScene("Menu");
    }
}
