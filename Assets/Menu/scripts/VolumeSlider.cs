﻿using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {
    public UnityEngine.UI.Slider slider;
    public UnityEngine.Audio.AudioMixer mixer;
    public string parameterName;

    void Start() {
        float savedVol = PlayerPrefs.GetFloat(parameterName, slider.value);
        SetVolume(savedVol);
        slider.value = savedVol;
        slider.onValueChanged.AddListener((float _) => SetVolume(_));
    }

    void SetVolume(float _value) {
        mixer.SetFloat(parameterName, ConvertToDecibel(_value));
        PlayerPrefs.SetFloat(parameterName, _value);
    }

    public float ConvertToDecibel(float _value) {
        return Mathf.Log10(Mathf.Max(_value, 0.0001f)) * 20f;
    }

    public void muteVoix(){
        mixer.SetFloat("Voix", ConvertToDecibel(0));
        PlayerPrefs.SetFloat("Voix", 0);
        GameObject.Find("Voix/Slider").GetComponent<Slider>().value = 0;
    }

    public void muteMusique(){
        mixer.SetFloat("Musique", ConvertToDecibel(0));
        PlayerPrefs.SetFloat("Musique", ConvertToDecibel(0));
        GameObject.Find("Musique/Slider").GetComponent<Slider>().value = 0;
    }
}