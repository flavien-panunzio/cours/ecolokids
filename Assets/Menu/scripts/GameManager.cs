﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour{
    public static GameManager instance = null;
    public bool menu = false;
    public bool pause = false;
    public bool disableClick = false;
    public bool disableDrag = true;
    public int vitesseTerre1;
    public int vitesseTerre2;
    public bool ifDrag = false;
    bool[] initfinLvl;
    int nbJeux = 8;
    bool reset;
    bool notNew;
    public Button btn;
    int count = 0;
    bool bateau = false;
    public GameObject Fin1;
    public GameObject Fin2;

    int lvl;
    string[] niveau = { "asie", "europe", "ameriqueNord", "oceanie", "ameriqueSud", "afrique", "groenland", "boat" };
    Vector3[] posContinent = {
            new Vector3(-14.665f, 10.367f, 29.983f),
            new Vector3(-12.365f, -65.898f, 50.067f),
            new Vector3(17.518f, 165.865f, 21.111f),
            new Vector3(26.98f, 64.505f, 73.635f),
            new Vector3(-32.819f, -139.907f, -1.924f),
            new Vector3(5.118f, -68.587f, 10.048f),
            new Vector3(21.024f, -118.483f, 73.826f),
            new Vector3(-9.88f, 147.04f, 25.68f)
        };
    EarthSpinScript earthspinscript;
    boat boat;
    Option option;
    public GameObject ResetProgression;
    GameObject continent;

    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance == this)
            Destroy(gameObject);
    }

    private void Start() {
        initPolution();
        InitGame();
        MusiqueManager.instance.PlayVoix("accueil");
    }

    void Update() {
        if (SceneManager.GetActiveScene().name == "Menu" && !reset) {
            for (int i = 0; i < initfinLvl.Length; i++) {
                if (PlayerPrefsX.GetBoolArray("FinLvl")[i] != initfinLvl[i]) {
                    notNew = true;
                    print("Nouveau niveau gagné, niveau : " + i);
                    if (i == 7){
                        count += 2;
                        bateau = true;
                    }
                    else
                        count++;
                    supprPolution(i);
                    initPolution(i);
                    initfinLvl[i] = PlayerPrefsX.GetBoolArray("FinLvl")[i];
                    Invoke("wait",2);
                }
            }
            if(!notNew)
                initPolution();
        }
        if (SceneManager.GetActiveScene().name != "Menu" && ResetProgression.activeSelf)
            ResetProgression.SetActive(false);
        else
            ResetProgression.SetActive(true);
    }

    void wait(){
        print(count);
        notNew = false;
        if (count == 7 && !bateau)
            finSansBateau();
        else if (count == 9)
            finFin();
    }

    void finSansBateau(){
        print("Fin sans le bateau");
        Fin1.SetActive(true);
        disableClick = true;
        Invoke("fin1", 8);
    }

    void fin1(){
        Fin1.SetActive(false);
        disableClick = false;
    }

    void finFin(){
        print("Fin Fin");
        Fin2.SetActive(true);
        disableClick = true;
        Invoke("fin2", 6);
    }

    void fin2(){
        Fin2.SetActive(false);
        disableClick = false;
    }

    void InitGame(){
        vitesseTerre1 = 10;
        vitesseTerre2 = 100;
        if (PlayerPrefsX.GetBoolArray("FinLvl").Length == 0)
            initPrefs();
        initfinLvl = new bool[nbJeux];
        initfinLvl = PlayerPrefsX.GetBoolArray("FinLvl");
        for (int i = 0; i< initfinLvl.Length; i++){
            if (initfinLvl[i])
                count++;
            if (initfinLvl[i] && i == 7){
                count++;
                bateau = true;
            }
        }
    }

    public void initPrefs(){
        bool[] tableau = new bool[nbJeux];
        for (int i = 0; i < nbJeux; i++)
            tableau[i] = false;
        PlayerPrefsX.SetBoolArray("FinLvl", tableau);
        for (int i = 0; i < niveau.Length; i++){
            GameObject continent = GameObject.Find("Terre/" + niveau[i]);
            if(continent.transform.childCount!= 0){
                for (int j = 0; j < continent.transform.childCount; j++){
                    GameObject child = continent.transform.GetChild(j).gameObject;
                    child.transform.localScale = Vector3.one;
                }
            }
        }
    }

    public void resetPrefs(){
        reset = true;
        GameObject.Find("DetectDrag").GetComponent<DragTerre>().resetPos();
        GameObject.Find("CanvasMainMenu").GetComponent<Menu>().clickRetour();
        initPrefs();
        initfinLvl = PlayerPrefsX.GetBoolArray("FinLvl");
        GameObject.Find("CanvasOptions").GetComponent<Option>().close();
        Invoke("waitReset", 2);
    }
    void waitReset(){
        reset = false;
    }

    void initPolution(int ignore = 10){
        bool[] prefs = PlayerPrefsX.GetBoolArray("FinLvl");
        for(int i=0; i< prefs.Length;i++){
            if (prefs[i] && i != ignore){
                GameObject continent = GameObject.Find("Terre/" + niveau[i]);
                if (continent.name != "boat"){
                    for (int j = 0; j < continent.transform.childCount; j++){
                        GameObject child = continent.transform.GetChild(j).gameObject;
                        child.transform.localScale = Vector3.zero;
                    }
                }
            }
        }
    }

    //Animation réussite ---------------------------------------------------------------
    void supprPolution(int _lvl){
        lvl = _lvl;
        earthspinscript = GameObject.Find("Terre").GetComponent<EarthSpinScript>();
        boat = GameObject.Find("boat").GetComponent<boat>();
        Invoke("delai1", 0.5f);
    }

    void delai1(){
        earthspinscript.speed = 0;
        boat.speed=0;
        continent = GameObject.Find("Terre/" + niveau[lvl]);
        LeanTween.rotate(GameObject.Find("Terre"), posContinent[lvl], 0.5f);
        Invoke("delai2", 1);
    }

    void delai2(){
        if (continent.name != "boat"){
            for (int i = 0; i < continent.transform.childCount; i++){
                GameObject child = continent.transform.GetChild(i).gameObject;
                LeanTween.scale(child, Vector3.zero, 1);
            }
        }
        Invoke("delai3", 1);
    }

    void delai3(){
        earthspinscript.speed = 20;
        boat.speed = 0.5f;
    }

    public void addEvent(UnityAction func){
        btn.onClick.AddListener(func);
    }
}