﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenAdjustPerspective : MonoBehaviour
{


    public bool AdjustRight;
    public float paddingRight = 0;
    public bool AdjustLeft;
    public float paddingLeft = 0;
    public bool AdjustTop;
    public float paddingTop = 0;
    public bool AdjustBottom;
    public float paddingBottom = 0;
    Vector3 wvp;
    float initCam;
    private void Start()
    {
        wvp = Camera.main.WorldToViewportPoint(transform.position);
        initCam = Camera.main.transform.position.z;
    }
    // Update is called once per frame
    void Update()
    {
        float ratio = Camera.main.transform.position.z / initCam;
        if (AdjustRight)
        {

            Vector3 rightLimit = Camera.main.ViewportToWorldPoint(new Vector3(0, wvp.y, Camera.main.transform.position.z));
            rightLimit.y = transform.position.y;
            rightLimit.z = transform.position.z;
            rightLimit.x = rightLimit.x - paddingRight * ratio;
            transform.position = rightLimit;
        }
        else if (AdjustLeft)
        {
            Vector3 leftLimit = Camera.main.ViewportToWorldPoint(new Vector3(1, wvp.y, Camera.main.transform.position.z));
            leftLimit.y = transform.position.y;
            leftLimit.z = transform.position.z;
            leftLimit.x = leftLimit.x + paddingLeft * ratio;
            transform.position = leftLimit;
        }

        if (AdjustTop)
        {
            Vector3 topLimit = Camera.main.ViewportToWorldPoint(new Vector3(wvp.x, 0, Camera.main.transform.position.z));
            topLimit.y = topLimit.y - paddingTop * ratio;
            topLimit.z = transform.position.z;
            topLimit.x = transform.position.x;
            transform.position = topLimit;
        }
        else if (AdjustBottom)
        {
            Vector3 bottomLimit = Camera.main.ViewportToWorldPoint(new Vector3(wvp.x, 1, Camera.main.transform.position.z));
            bottomLimit.y = bottomLimit.y + paddingBottom * ratio;
            bottomLimit.z = transform.position.z;
            bottomLimit.x = transform.position.x;
            transform.position = bottomLimit;
        }

    }
}