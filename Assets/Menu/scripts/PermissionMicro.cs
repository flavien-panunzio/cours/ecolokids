using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.iOS;

public class PermissionMicro : MonoBehaviour {

    IEnumerator Start()
    {
        FindMicrophones();

        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            Debug.Log("Microphone found");
        }
        else
        {
            Debug.Log("Microphone not found");
        }

    }

    void FindMicrophones()
    {
        foreach (var device in Microphone.devices)
        {
            Debug.Log("Name: " + device);
        }
    }
}
