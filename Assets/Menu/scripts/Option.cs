﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Option : MonoBehaviour{
    public GameObject options;
    public GameObject content;

    public void clickOptions(){
        MusiqueManager.instance.pauseAll();
        GameManager.instance.disableClick = true;
        GameManager.instance.disableDrag = true;
        options.SetActive(true);
        if (Time.timeScale == 0)
            options.transform.localScale = Vector3.one;
        else{
            if (SceneManager.GetActiveScene().name == "Menu"){
                
                LeanTween.scale(options, Vector3.one, 0.5f).setEaseInCirc().setOnUpdate(posContent);
            }
            else
                options.transform.localScale = Vector3.one;
        }
    }

    void posContent(float test){
        content.transform.localPosition = Vector3.zero;
    }

    public void close(){
        GameManager.instance.disableClick = false;
        GameManager.instance.disableDrag = false;
        MusiqueManager.instance.playAll();
        if (Time.timeScale == 0){
            options.transform.localScale = Vector3.zero;
            unactive();
        }else
            LeanTween.scale(options, Vector3.zero, 0.5f).setEaseInCirc().setOnComplete(unactive);
    }

    void unactive(){
        options.SetActive(false);
    }
}
