﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Continent : MonoBehaviour{
    public string Continents = "";

    private void OnMouseUp(){
        if (!GameManager.instance.disableClick && !GameManager.instance.disableDrag && !GameManager.instance.ifDrag)
        {
            MusiqueManager.instance.stopAll();
            SceneManager.LoadScene(Continents);
        }
        GameManager.instance.ifDrag = false;
    }
}