﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DragTerre : MonoBehaviour{
    public EarthSpinScript earthspinscript;
    public GameObject Terre;
    public Vector3 _mouseReference;
    private Vector3 _rotation;
    public Vector3 init;

    private void Awake(){
        init = Terre.transform.localEulerAngles;
    }

    void Start(){
        _rotation = Terre.transform.localEulerAngles;
    }

    private void OnMouseDown() {
        if (!GameManager.instance.disableDrag){
            earthspinscript.speed = 0;
            _mouseReference = Input.mousePosition;
        }
    }

    private void OnMouseUp(){
        if (!GameManager.instance.disableDrag){
            earthspinscript.speed = GameManager.instance.vitesseTerre1;
            GameManager.instance.ifDrag = false;
        }
    }

    void OnMouseDrag(){
        if (_mouseReference != Input.mousePosition && !GameManager.instance.disableDrag){
            GameManager.instance.ifDrag = true;
            _rotation.y = -(Input.mousePosition - _mouseReference).x * 0.2f;
            _rotation.x = (Input.mousePosition - _mouseReference).y * 0.2f;
            Terre.transform.Rotate(Vector3.up, _rotation.y, Space.World);
            Terre.transform.Rotate(new Vector3(1,0,0), _rotation.x, Space.World);
            _mouseReference = Input.mousePosition;
        }
    }

    public void resetPos(){
        LeanTween.rotate(Terre, init, 0.5f);
    }
}