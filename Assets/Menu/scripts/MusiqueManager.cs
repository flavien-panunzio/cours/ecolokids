﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusiqueManager : MonoBehaviour{
    public static MusiqueManager instance = null;
    string _nomMusique;
    bool[] _isPlay;
    AudioSource[] PistesAudio;
    GameObject Voix;

    public string NomMusique {
        set{
            _nomMusique = value;
            PlaySingle(_nomMusique);
        }get{
            return _nomMusique;
        }
    }

    void Awake(){
        if (instance == null)
            instance = this;
        else
            Object.Destroy(gameObject);
        PistesAudio = GetComponentsInChildren<AudioSource>();
        _isPlay = new bool[PistesAudio.Length];
    }

    public void pauseAll(){
        for (int i = 0; i < PistesAudio.Length; i++)
            PistesAudio[i].Pause();
    }

    public void stopAll(){
        for (int i = 0; i < PistesAudio.Length; i++)
            PistesAudio[i].Stop();
    }

    public void playAll(){
        for (int i = 0; i < PistesAudio.Length; i++){
            if(_isPlay[i])
                PistesAudio[i].Play();
        }
    }

    void PlaySingle(string name){
        for (int i = 0; i < PistesAudio.Length; i++){
            PistesAudio[i].Stop();
            _isPlay[i] = false;
            if (PistesAudio[i].gameObject.name == name){
                PistesAudio[i].Play();
                _isPlay[i] = true;
            }
        }
    }

    public void PlayVoix(string name){
        AudioSource[] Stop = GameObject.Find("CanvasMainMenu/Voix").gameObject.GetComponentsInChildren<AudioSource>();
        for (int i = 0; i < Stop.Length; i++){
            Stop[i].Stop();
        }
        GameObject Voix = GameObject.Find("CanvasMainMenu/Voix/" + name).gameObject;
        Voix.GetComponent<AudioSource>().Play();
    }

    void Update(){
        for(int i = 0; i < PistesAudio.Length; i++){
            if (PistesAudio[i].timeSamples == PistesAudio[i].clip.samples && !PistesAudio[i].loop)
                _isPlay[i] = false;
        }
    }

    public void preEcoute(string son){
        if (son=="Voix")
            PistesAudio[0].Play();
        else
            PistesAudio[1].Play();
    }

    public void preEcouteStop(){
        PistesAudio[0].Stop();
        PistesAudio[1].Stop();
    }
}