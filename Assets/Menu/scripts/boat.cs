﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boat : MonoBehaviour{
    public GameObject BoatEmpty;
    public Transform Boat;
    GameObject[] child;
    public float speed = 0.5f;
    int count = 0;
    int rotate;

    void Start(){
        child = new GameObject[BoatEmpty.transform.childCount];
        for (int i = 0; i < BoatEmpty.transform.childCount; i++){
            child[i] = BoatEmpty.transform.GetChild(i).gameObject;
            child[i].transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void Update(){
        var step = speed * Time.deltaTime;
        LeanTween.rotate(Boat.gameObject, child[rotate].transform.eulerAngles, 0.2f);
        Boat.position = Vector3.MoveTowards(Boat.position, child[count].transform.position, step);

        rotate = count - 1;
        if (count == 0)
            rotate = BoatEmpty.transform.childCount-1;

        if (child[count].transform.position == Boat.position){
            count++;
            if (child.Length == count)
                count=0;
        }
    }
}
