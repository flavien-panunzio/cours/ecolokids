﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PreEcoute : EventTrigger
{
    public string son;

    public override void OnPointerDown(PointerEventData eventData){
        MusiqueManager.instance.preEcoute(this.transform.parent.name);
    }

    public override void OnPointerUp(PointerEventData data){
        MusiqueManager.instance.preEcouteStop();
    }
}