﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenAdjust : MonoBehaviour{


    public bool AdjustRight;
    public float paddingRight = 0;
    public bool AdjustLeft;
    public float paddingLeft = 0;
    public bool AdjustTop;
    public float paddingTop = 0;
    public bool AdjustBottom;
    public float paddingBottom = 0;


    // Update is called once per frame
    void Update()
    {
        if (AdjustRight)
        {
            Vector3 rightLimit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
            rightLimit.y = transform.position.y;
            rightLimit.z = transform.position.z;
            rightLimit.x = rightLimit.x - paddingRight;
            transform.position = rightLimit;
        }
        else if (AdjustLeft)
        {
            Vector3 leftLimit = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
            leftLimit.y = transform.position.y;
            leftLimit.z = transform.position.z;
            leftLimit.x = leftLimit.x + paddingLeft;
            transform.position = leftLimit;
        }

        if (AdjustTop)
        {
            Vector3 topLimit = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
            topLimit.y = topLimit.y - paddingTop;
            topLimit.z = transform.position.z;
            topLimit.x = transform.position.x;
            transform.position = topLimit;
        }
        else if (AdjustBottom)
        {
            Vector3 bottomLimit = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
            bottomLimit.y = bottomLimit.y + paddingBottom;
            bottomLimit.z = transform.position.z;
            bottomLimit.x = transform.position.x;
            transform.position = bottomLimit;
        }

    }
}