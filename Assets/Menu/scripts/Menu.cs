﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour{
    public GameObject lettres;
    public GameObject btnjouer;
    public GameObject terre;
    public GameObject retour;
    public GameObject consignes;
    public GameObject reset;
    public GameObject animRotate;
    public EarthSpinScript earthspinscript;

    void Start(){
        GameManager.instance.pause = false;
        GameManager.instance.disableClick = true;
        MusiqueManager.instance.NomMusique = "Menu";
        if (GameManager.instance.menu)
            returnMenu();
    }

    public void returnMenu(){
        lettres.SetActive(false);
        btnjouer.SetActive(false);
        GameManager.instance.disableDrag = false;
        GameManager.instance.disableClick = false;
        LeanTween.scale(retour, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(consignes, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(reset, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(animRotate, new Vector3(0.2f,0.2f,0.2f), 0.5f).setEaseInCirc();
        LeanTween.move(Camera.main.gameObject, new Vector3(0, 0, -3), 0.5f).setEaseInCirc().setOnComplete(changespeed);
        earthspinscript.speed = GameManager.instance.vitesseTerre2;
    }

    public void clickJouer(){
        GameManager.instance.disableDrag = false;
        GameManager.instance.disableClick = false;
        GameManager.instance.menu = true;
        LeanTween.scale(lettres, Vector3.zero, 0.5f).setEaseInCirc();
        LeanTween.scale(retour, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(consignes, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(reset, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(animRotate, new Vector3(0.2f, 0.2f, 0.2f), 0.5f).setEaseInCirc();
        LeanTween.scale(btnjouer, Vector3.zero, 0.5f).setEaseInCirc().setOnComplete(unactive);
        LeanTween.move(Camera.main.gameObject, new Vector3(0, 0, -3), 0.5f).setEaseInCirc().setOnComplete(changespeed);
        earthspinscript.speed = GameManager.instance.vitesseTerre2;
        MusiqueManager.instance.PlayVoix("continents");
    }

    public void clickRetour(){
        GameManager.instance.disableDrag = true;
        GameManager.instance.disableClick = true;
        GameManager.instance.menu = false;
        lettres.SetActive(true);
        btnjouer.SetActive(true);
        LeanTween.scale(lettres, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(retour, Vector3.zero, 0.5f).setEaseInCirc();
        LeanTween.scale(consignes, Vector3.zero, 0.5f).setEaseInCirc();
        LeanTween.scale(reset, Vector3.zero, 0.5f).setEaseInCirc();
        LeanTween.scale(btnjouer, Vector3.one, 0.5f).setEaseInCirc();
        LeanTween.scale(animRotate, Vector3.zero, 0.5f).setEaseInCirc();
        LeanTween.move(Camera.main.gameObject, new Vector3(0, 0, -7), 0.5f).setEaseInCirc().setOnComplete(changespeed);
        earthspinscript.speed = GameManager.instance.vitesseTerre2;
        MusiqueManager.instance.PlayVoix("accueil");
    }

    public void lunchOptionsPannel(){
        GameObject.Find("DontDestroyOnLoad/CanvasOptions").GetComponent<Option>().clickOptions();
    }

    public void changespeed(){
        earthspinscript.speed = GameManager.instance.vitesseTerre1;
    }

    public void unactive(){
        lettres.SetActive(false);
        btnjouer.SetActive(false);
    }
}
