﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour{
    public GameObject BtnPause;
    public GameObject panel;
    public GameObject panelOptions;

    void Update(){
        Visible();
    }

    public void clickPause(){
        panel.SetActive(true);
        GameManager.instance.disableClick = true;
        PauseGame();
    }

    public void clickContinuer(){
        panel.SetActive(false);
        GameManager.instance.disableClick = false;
        ContinueGame();
    }

    public void clickMenu(){
        panel.SetActive(false);
        GameManager.instance.disableClick = false;
        ContinueGame();
        SceneManager.LoadScene("Menu");
    }

    public void Visible(){
        if (GameManager.instance.pause)
            BtnPause.SetActive(true);
        else
            BtnPause.SetActive(false);
    }

    private void PauseGame(){
        Time.timeScale = 0;
        MusiqueManager.instance.pauseAll();
    }

    private void ContinueGame(){
        Time.timeScale = 1;
        MusiqueManager.instance.playAll();
    }

    public void rejouer(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        panel.SetActive(false);
        GameManager.instance.disableClick = false;
        ContinueGame();
    }
}