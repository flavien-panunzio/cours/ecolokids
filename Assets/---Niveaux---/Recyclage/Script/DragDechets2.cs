﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDechets2 : MonoBehaviour
{

    //  public GameObject dragZone;
       public GameObject GarconContent;
       public GameObject GarconPasContent;

    public GameObject poubelleFerme;
    public GameObject poubelleOuverte;

    public GameObject ObjPbBleu;
    public GameObject ObjPbJaune;
    public GameObject ObjPbVerte;
    public GameObject ObjPbMarron;
    public GameObject ObjPbNoire;
    public GameObject check;
    public GameObject faux;

    public int pbBleu = 0 ;
    public int pbJaune = 0 ;
    public int pbVerte = 0 ;
    public int pbMarron = 0 ;
    public int pbNoir = 0 ;



    Vector3 _initPos;
    Vector3 objPosition;

    public int valuePuzzle;


    void Start()
    {
        _initPos = transform.position;
        valuePuzzle = 1;
        GarconContent.SetActive(false);
        GarconPasContent.SetActive(true);

        ObjPbBleu.SetActive(false);
        ObjPbJaune.SetActive(false);
        ObjPbVerte.SetActive(false);
        ObjPbMarron.SetActive(false);
        ObjPbNoire.SetActive(false);

    }



    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
    }



    private void OnMouseUp()
    {

        if (pbBleu ==1)
        {
            ObjPbBleu.SetActive(true) ; 
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }
        else if (pbJaune == 1)
        {

            ObjPbJaune.SetActive(true);
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }
        else if (pbVerte == 1)
        {

            ObjPbVerte.SetActive(true);
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }
        else if (pbMarron == 1)
        {

            ObjPbMarron.SetActive(true);
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }
        else if (pbNoir == 1)
        {

            ObjPbNoire.SetActive(true);
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }

        else if (pbBleu == 6)
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
            valuePuzzle = 0;
            poubelleFerme.SetActive(true);
            poubelleOuverte.SetActive(false);
            check.GetComponent<AudioSource>().Play();
        }
        else if (pbJaune== 6)
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
            valuePuzzle = 0;
            poubelleFerme.SetActive(true);
            poubelleOuverte.SetActive(false);
            check.GetComponent<AudioSource>().Play();
        }
        else if (pbVerte == 6)
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
            valuePuzzle = 0;
            poubelleFerme.SetActive(true);
            poubelleOuverte.SetActive(false);
            check.GetComponent<AudioSource>().Play();
        }
        else if (pbMarron == 6)
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
            valuePuzzle = 0;
            poubelleFerme.SetActive(true);
            poubelleOuverte.SetActive(false);
            check.GetComponent<AudioSource>().Play();
        }
        else if (pbNoir == 6)
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
            valuePuzzle = 0;
            poubelleFerme.SetActive(true);
            poubelleOuverte.SetActive(false);
            check.GetComponent<AudioSource>().Play();
        }



        else
        {
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);
            faux.GetComponent<AudioSource>().Play();
        }

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "bleu")
        {
            pbBleu += 1; 
        }

        else if (collision.gameObject.name == "jaune")
        {
            pbJaune += 1;
        }

        else if(collision.gameObject.name == "vert")
        {
            pbVerte += 1;
        }

        else if (collision.gameObject.name == "marron")
        {
            pbMarron += 1;
        }

        else if (collision.gameObject.name == "noir")
        {
            pbNoir += 1;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "bleu")
        {
            pbBleu -= 1;
        }

        else if (collision.gameObject.name == "jaune")
        {
            pbJaune -= 1;
        }

        else if (collision.gameObject.name == "vert")
        {
            pbVerte -= 1;
        }

        else if (collision.gameObject.name == "marron")
        {
            pbMarron -= 1;
        }

        else if (collision.gameObject.name == "noir")
        {
            pbNoir -= 1;
        }
    }




}
