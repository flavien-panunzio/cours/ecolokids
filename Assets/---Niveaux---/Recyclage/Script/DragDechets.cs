﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDechets : MonoBehaviour
{

    public GameObject dragZone;
    public GameObject GarconContent;
    public GameObject GarconPasContent;

    Vector3 _initPos;
    Vector3 objPosition;
    Vector2 _VectDragZone;
    float _distDragZone;

    public int valuePuzzle;


    void Start()
    {
        _initPos = transform.position;
        valuePuzzle = 1;
        GarconContent.SetActive(false);
        GarconPasContent.SetActive(true);
    }

    void Update()
    {
        float _vectX = dragZone.transform.position.x - this.transform.position.x;
        float _vectY = dragZone.transform.position.y - this.transform.position.y;

        _distDragZone = Mathf.Sqrt((_vectX * _vectX) + (_vectY * _vectY));
    }

    private void OnMouseDrag()
    {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = objPosition;
    }



    private void OnMouseUp()
    {

        if (_distDragZone >= 2)
        {
            transform.position = _initPos;
            GarconContent.SetActive(false);
            GarconPasContent.SetActive(true);

        }

        else
        {
            GarconContent.SetActive(true);
            GarconPasContent.SetActive(false);
            this.GetComponent<SpriteRenderer>().sortingOrder = -10;
            gameObject.SetActive(false);
          
            valuePuzzle = 0;
        }


    }

}
