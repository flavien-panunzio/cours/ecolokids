﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeDechets : MonoBehaviour
{


    public GameObject debut;
    public GameObject ZoneClic;
    public GameObject[] ZoneClic2;
    public GameObject[] overDechets;
    



  
        


        // Use this for initialization
        void Start()
    {
        ZoneClic.SetActive(false);
        debut.SetActive(true);
        for (int i = 0; i < overDechets.Length; i++)
        {
            overDechets[i].GetComponent<BoxCollider2D>().enabled = false;
        }
        for (int i = 0; i < ZoneClic2.Length; i++)
        {
            ZoneClic2[i].GetComponent<BoxCollider2D>().enabled = false;
        }

        GameManager.instance.pause = false;

        MusiqueManager.instance.NomMusique = "Jeu";
    }



    private void OnMouseDown()
    {
        GameManager.instance.pause = true; 

        
        debut.SetActive(false);
        ZoneClic.SetActive(true);


        for (int i = 0; i < overDechets.Length; i++)
        {
            overDechets[i].GetComponent<BoxCollider2D>().enabled = true;
        }
        for (int i = 0; i < ZoneClic2.Length; i++)
        {
            ZoneClic2[i].GetComponent<BoxCollider2D>().enabled = true;
        }

    }

}
