﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinRecyclage : MonoBehaviour

{
    public GameObject fin;
    public GameObject[] zoneClic;

    public DragDechets2[] resDechet;
    private int resultat = 10;


    
    void Start()
    {
        fin.SetActive(false);
    }

    
    void Update()
    {
        resultat = resDechet[0].valuePuzzle + resDechet[1].valuePuzzle + resDechet[2].valuePuzzle + resDechet[3].valuePuzzle + resDechet[4].valuePuzzle + resDechet[5].valuePuzzle + 
            resDechet[6].valuePuzzle + resDechet[7].valuePuzzle + resDechet[8].valuePuzzle + resDechet[9].valuePuzzle;

      // Debug.Log(resultat);


        if (resultat == 0)
        {

            fin.SetActive(true);
            GameManager.instance.pause = false;

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[4] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);

        }

        if (GameManager.instance.disableClick == true)
        {
            for (int i = 0; i < zoneClic.Length; i++)
            {
                zoneClic[i].GetComponent<BoxCollider2D>().enabled = false;
            }


        }
        else
        {
            for (int i = 0; i < zoneClic.Length; i++)
            {
                zoneClic[i].GetComponent<BoxCollider2D>().enabled = true;
            }

        } 

    }

}
