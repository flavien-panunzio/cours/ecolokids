﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneDepotDechets : MonoBehaviour
{
    public GameObject poubelleFerme;
    public GameObject poubelleOuverte;


    // Start is called before the first frame update
    void Start()
    {
        poubelleFerme.SetActive(true) ;
        poubelleOuverte.SetActive(false);
    }
    


    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        {
            poubelleFerme.SetActive(false);
            poubelleOuverte.SetActive(true);
        }

        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        poubelleFerme.SetActive(true);
        poubelleOuverte.SetActive(false);
    }
}
