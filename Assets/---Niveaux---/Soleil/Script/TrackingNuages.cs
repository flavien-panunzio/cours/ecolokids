﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingNuages : MonoBehaviour {

    public GameObject NuageGris ;

    float posX;
    float posY;
    public int orientation;

    // Use this for initialization
    void Start () {


        if (orientation == 0)
        {
            posX = NuageGris.transform.position.x;
            posY = NuageGris.transform.position.y - 0.750999f;
        }

        if (orientation == 1)
        {
            posX = NuageGris.transform.position.x + 0.750999f;
            posY = NuageGris.transform.position.y;
        }

        if (orientation == 2)
        {
            posX = NuageGris.transform.position.x;
            posY = NuageGris.transform.position.y - 0.765f;
        }


    }
	
	// Update is called once per frame
	void Update () {
        if (orientation == 0)
        {
            posX = NuageGris.transform.position.x;
            posY = NuageGris.transform.position.y - 0.750999f;
        }

        if (orientation == 1)
        {
            posX = NuageGris.transform.position.x + 0.750999f;
            posY = NuageGris.transform.position.y;
        }

        if (orientation == 2)
        {
            posX = NuageGris.transform.position.x;
            posY = NuageGris.transform.position.y - 0.765f;
        }


        Vector3 objPosition = new Vector3(posX, posY, 1);

        transform.position = objPosition;
    }
}
