﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closeNuages : MonoBehaviour
{


    public GameObject debutPanneau;


    public GameObject[] Nuages;


    // Use this for initialization
    void Start()
    {
        debutPanneau.SetActive(true);
        GameManager.instance.pause = false;

        for (int i = 0; i < Nuages.Length; i++)
        {
            Nuages[i].GetComponent<BoxCollider2D>().enabled = false;
        }


        MusiqueManager.instance.NomMusique = "Jeu";

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {

        debutPanneau.SetActive(false);
        GameManager.instance.pause = true;
        for (int i = 0; i < Nuages.Length; i++)
        {
            Nuages[i].GetComponent<BoxCollider2D>().enabled = true;
        }
    }

}

