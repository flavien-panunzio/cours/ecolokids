﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour {

    public enum DirectionAxes
    {
        X,
        Y
    }

    public DirectionAxes Axe;
    Vector3 _initPos;
    int _dragDirection = 0; //0 left, 1 right ou 0 up, 1 down
    Vector2 _lastPosition;

    public LayerMask RaycastTargetLayers;

	void Start () {
        //on garde en référence la position de départ
        _initPos = transform.position;
        _initPos.z = 0;

    }

    private void OnMouseDown()
    {
        //on change la layer du block en cours
        gameObject.layer = LayerMask.NameToLayer("CurrentBlock");
        _lastPosition = gameObject.transform.position;
    }

    private void OnMouseDrag()
    {
        
        if (Axe == DirectionAxes.X)
        {

           

            Vector3 convertedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            convertedPos = new Vector3(convertedPos.x, _initPos.y, _initPos.z);
             Vector3 Center = new Vector3(transform.position.x, transform.position.y - gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2 , transform.position.z);
            //ici on lance deux rayon de chaque côté afin de savoir à quel endroit arrêter le drag
            RaycastHit2D hitLeft = Physics2D.Raycast(Center, -Vector2.right, 100f, RaycastTargetLayers);
            RaycastHit2D hitRight = Physics2D.Raycast(Center, Vector2.right, 100f, RaycastTargetLayers);

            //afin de voir les rayons, on affiche une vue debug temporaire (à enlever au final)
            Debug.DrawRay(Center, -Vector2.right * hitLeft.distance, Color.green);
            Debug.DrawRay(Center, Vector2.right * hitRight.distance, Color.red);

            //il faut savoir dans quele direction on drag la pièce
            if (convertedPos.x < _lastPosition.x)
            {
                _dragDirection = 0;//left
            }else if (convertedPos.x > _lastPosition.x)
            {
                _dragDirection = 1;//right
            }


            if (hitLeft.collider != null && _dragDirection == 0)
            {
                if (convertedPos.x > hitLeft.point.x + gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2)
                {
                    transform.position = convertedPos;
                }
                else
                {
                    transform.position = new Vector3(hitLeft.point.x + gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2, _initPos.y, _initPos.z);
                }
            }


            if (hitRight.collider != null && _dragDirection == 1)
            {
                if (convertedPos.x < hitRight.point.x - gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2)
                {
                    transform.position = convertedPos;
                }
                else
                {
                    transform.position = new Vector3(hitRight.point.x - gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2, _initPos.y, _initPos.z);
                }
            }
            
            
        }

        else if (Axe == DirectionAxes.Y)
        {

            Vector3 convertedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            convertedPos = new Vector3(_initPos.x, convertedPos.y, _initPos.z);
            Vector3 Center = new Vector3(transform.position.x + gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2, transform.position.y , transform.position.z);

            //ici on lance deux rayon de chaque côté afin de savoir à quel endroit arrêter le drag
            RaycastHit2D hitLeft = Physics2D.Raycast(Center, -Vector2.up, 100f, RaycastTargetLayers);
            RaycastHit2D hitRight = Physics2D.Raycast(Center, Vector2.up, 100f, RaycastTargetLayers);

            //afin de voir les rayons, on affiche une vue debug temporaire (à enlever au final)
            Debug.DrawRay(Center, -Vector2.up * hitLeft.distance, Color.green);
            Debug.DrawRay(Center, Vector2.up * hitRight.distance, Color.red);

            //il faut savoir dans quele direction on drag la pièce
            if (convertedPos.y < _lastPosition.y)
            {
                _dragDirection = 0;//left
            }
            else if (convertedPos.y > _lastPosition.y)
            {
                _dragDirection = 1;//right
            }


            if (hitLeft.collider != null && _dragDirection == 0)
            {
                if (convertedPos.y > hitLeft.point.y + gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2)
                {
                    transform.position = convertedPos;
                }
                else
                {
                    transform.position = new Vector3(_initPos.x, hitLeft.point.y + gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2,  _initPos.z);
                }
            }


            if (hitRight.collider != null && _dragDirection == 1)
            {
                if (convertedPos.y < hitRight.point.y - gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2)
                {
                    transform.position = convertedPos;
                }
                else
                {
                    transform.position = new Vector3(_initPos.x, hitRight.point.y - gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2,  _initPos.z);
                }
            }


        }

        else
        {

        }

        _lastPosition = gameObject.transform.position;
    }

    private void OnMouseUp()
    { 
        //on change la layer du block en cours pour la remettre au layer par défaut des blocks
        gameObject.layer = LayerMask.NameToLayer("Block");
    }
}
