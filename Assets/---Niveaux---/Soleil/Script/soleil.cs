﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soleil : MonoBehaviour
{

    // float distance = 10;


    public GameObject gagneObj;
    public GameObject check;
    
    public GameObject[] Nuages;

    public GameObject nuagesBlanc;
    public GameObject nuagesNoir;
    float PosX;

  //  private BoxCollider2D Box2D;


    // Use this for initialization
    void Start()
    {
        PosX = gameObject.transform.position.x;
        gagneObj.SetActive(false);
        nuagesBlanc.SetActive(false);
      
    }

    // Update is called once per frame


    private void Update()
    { 
        if (transform.position.y <= -1.3f)
        {
            check.GetComponent<AudioSource>().Play();
        }
        if (transform.position.y <= -1.33f)
        {
            transform.position = new Vector3(PosX, -1.333f, 1);
            gagneObj.SetActive(true);
            nuagesBlanc.SetActive(true);
            nuagesNoir.SetActive(false);
            GameManager.instance.pause = false;

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[3] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);




        }

        if (GameManager.instance.disableClick == true)
        {
            for (int i = 0; i < Nuages.Length; i++)
            {
                Nuages[i].GetComponent<BoxCollider2D>().enabled = false;
            }

        }
        else
        {
            for (int i = 0; i < Nuages.Length; i++)
            {
                Nuages[i].GetComponent<BoxCollider2D>().enabled = true;
            }

        }


    }
    
}

