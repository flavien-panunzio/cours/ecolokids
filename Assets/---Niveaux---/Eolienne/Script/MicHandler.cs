﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MicHandler : MonoBehaviour
{
    AudioClip microphoneInput;
    bool microphoneInitialized;
    public float sensitivity;
    public closeEolienne _eolienne;

    

    private float _compteurTours = 0;
    private float _compteurAngle = 0;
    private float _lastRotation;
    private float _nowRotation;
    private float _diffRotation;
    private int vitesse = 0;

    public GameObject ampoule1;
    public GameObject ampoule2;
    public GameObject ampoule3;
    public GameObject ampoule4;
    public GameObject ampoule5;
    public GameObject ampoule6;
    public GameObject ampoule7;
    public GameObject ampoule8;
    public GameObject ampoule9;
    public GameObject fin;
    public GameObject vent;
    public GameObject check;




    private float _sensitivity;
    private Vector3 _mouseReference;
    private Vector3 _mouseOffset;
    private Vector3 _rotation;
    private bool _isRotating;


    private void Start()
    {
        _lastRotation = transform.rotation.z;

        ampoule1.SetActive(true);
        ampoule2.SetActive(false);
        ampoule3.SetActive(false);
        ampoule4.SetActive(false);
        ampoule5.SetActive(false);
        ampoule6.SetActive(false);
        ampoule7.SetActive(false);
        ampoule8.SetActive(false);
        ampoule9.SetActive(false);
        fin.SetActive(false);

        _sensitivity = 0.4f;
        _rotation = Vector3.zero;

        this.GetComponent<CircleCollider2D>().enabled = false;
    }


    private void Awake()
    {
        if (Microphone.devices.Length > 0)
        {
            microphoneInput = Microphone.Start(Microphone.devices[0], true, 999, 44100);
            microphoneInitialized = true;
        }

    }


    private bool micro = true;

    IEnumerator GetMicrophone()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            micro = true;
        }
        else
        {
            micro = false;
        }
    } 

    
    private void Update()
    {


          if (micro)
          { 
        int dec = 128;
             float[] waveData = new float[dec];
             int micPosition = Microphone.GetPosition(null) - (dec + 1);
             microphoneInput.GetData(waveData, micPosition);

             float levelMax = 0;
             for (int i = 0; i < dec; i++)
             {
                 float wavePeak = waveData[i] * waveData[i];
                 if (levelMax < wavePeak)
                 {
                     levelMax = wavePeak;
                 }
             }
             float level = Mathf.Sqrt(Mathf.Sqrt(levelMax));



             if (level > sensitivity && _eolienne.flapped == true && _ClickOk == true)
             {
                 vitesse = 500;
                 vent.GetComponent<AudioSource>().Play();
             }

             if (level < sensitivity && _eolienne.flapped == true && _ClickOk == true)
             {
                 vitesse = vitesse - 10;
                 if (vitesse < 0)
                 {
                     vitesse = 0;
                 }
             } 


      


        transform.Rotate(0, 0, -Time.deltaTime * vitesse);


            _nowRotation = transform.rotation.z;
            _diffRotation = (_lastRotation - _nowRotation) * 1000;
            if (_diffRotation < 0)
            {
                _diffRotation = 360 - _lastRotation - _nowRotation;
            }
            _compteurAngle = _compteurAngle + _diffRotation;
            _compteurTours = _compteurAngle / 360;

            _lastRotation = _nowRotation;
       }
       else
        {

           this.GetComponent<CircleCollider2D>().enabled = true;

            if (_isRotating)
            {
                _mouseOffset = (Input.mousePosition - _mouseReference);
                _rotation.z = -(_mouseOffset.x + _mouseOffset.y) * _sensitivity;
                transform.Rotate(_rotation);
                _mouseReference = Input.mousePosition;
            }

            float rotZ = transform.rotation.z;

            if (rotZ < 0.1 & rotZ > -0.1)
            {
                _compteurTours++;
            }
        } 

        


        if (_compteurTours >= 10)
        {
            ampoule2.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 20)
        {
            ampoule3.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 30)
        {
            ampoule4.SetActive(true);
            check.GetComponent<AudioSource>().Play();
        }

        if (_compteurTours >= 40)
        {
            ampoule5.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 50)
        {
            ampoule6.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 60)
        {
            ampoule7.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 70)
        {
            ampoule8.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }

        if (_compteurTours >= 80)
        {
            ampoule9.SetActive(true);
            check.GetComponent<AudioSource>().Play();

        }


        if (_compteurTours >= 90)
        {
            fin.SetActive(true);
            check.GetComponent<AudioSource>().Play();

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[2] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);

            GameManager.instance.pause = false;
            _eolienne.flapped = false;
        }

        if (GameManager.instance.disableClick == true)
        {
            _ClickOk = false; 
         
        }
        else if (GameManager.instance.disableClick == false)
        {
            _ClickOk = true;

        }


    }

    private bool _ClickOk;

    public Transform target;

   void OnMouseDown()
    {
        _isRotating = true;
        _mouseReference = Input.mousePosition;
    }

    void OnMouseUp()
    {
        _isRotating = false;
    }
}