﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragPuzzle : MonoBehaviour
{

    Vector3 _initPos;
    Vector3 objPosition;

    public GameObject dragZone;
    public GameObject SonFaux;
    public GameObject SonCheck;

    RaycastHit2D hitLeft ;
    RaycastHit2D hitRight;

    Vector2 _VectDragZone;

    private float distanceRayon;

    public LayerMask RaycastTargetLayers;


    private bool dragOk = true;

    public int valuePuzzle = 0;



    // Start is called before the first frame update
    void Start()
    {
        _initPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        _VectDragZone = new Vector2(dragZone.transform.position.x - this.transform.position.x, dragZone.transform.position.y - this.transform.position.y);

    }

    private void OnMouseDrag()
    {

        if (dragOk == true)
        {

            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = objPosition;

            RaycastHit2D hitRight = Physics2D.Raycast(objPosition, _VectDragZone, 100f, RaycastTargetLayers);

            //afin de voir les rayons, on affiche une vue debug temporaire (à enlever au final)

            Debug.DrawRay(objPosition, _VectDragZone, Color.blue);

            // Debug.Log("Dist : " + hitRight.distance);

            distanceRayon = hitRight.distance;

        }

    }
     


    private void OnMouseUp()
    {

        if (distanceRayon != 0)
        {
            transform.position = _initPos;
            SonFaux.GetComponent<AudioSource>().Play();


        }

        else
        {
            transform.position =  dragZone.transform.position;
            dragOk = false;
            valuePuzzle = 1;
            SonCheck.GetComponent<AudioSource>().Play();

        }


    }

}
