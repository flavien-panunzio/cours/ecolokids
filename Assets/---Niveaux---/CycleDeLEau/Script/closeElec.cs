﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closeElec : MonoBehaviour
{


    public GameObject debut;
    public GameObject[] zoneClic;

     

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < zoneClic.Length; i++)
        {
            zoneClic[i].GetComponent<BoxCollider2D>().enabled = false;
        }
        debut.SetActive(true);

        GameManager.instance.pause = false;
        MusiqueManager.instance.NomMusique = "Jeu";
    }

    // Update is called once per frame
    void Update()
    {





    }

    private void OnMouseDown()
    {

        debut.SetActive(false);
        for (int i = 0; i < zoneClic.Length; i++)
        {
            zoneClic[i].GetComponent<BoxCollider2D>().enabled = true;
        }
        GameManager.instance.pause = true;
    }

}
