﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class SkipVideo : MonoBehaviour
{

    public GameObject panneauFinal;
    public GameObject video;
    public GameObject video1;
    public GameObject video2;

    // Start is called before the first frame update
    void Start()
    {
        panneauFinal.SetActive(false);
        video.SetActive(true);
        video1.SetActive(true);
        video2.SetActive(true);


        video2.GetComponent<VideoPlayer>().loopPointReached += CheckFin ;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Rejouer()
    {
        panneauFinal.SetActive(true);
        video.SetActive(false);
        video1.SetActive(false);
        video2.SetActive(false);
    }

    void CheckFin(UnityEngine.Video.VideoPlayer vp)
    {
        panneauFinal.SetActive(true);
        video.SetActive(false);
        video1.SetActive(false);
        video2.SetActive(false);
    }
}
