﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chambreWin : MonoBehaviour {


    public GameObject fin;
    public GameObject zoneClic;
    public GameObject SonCheck;


    public chambre resChambre1;
    public chambre resChambre2;
    public chambre resChambre3;
    public chambre resChambre4;
    public chambre resChambre5;
    public chambre resChambre6;
    public chambre resChambre7;
    public chambre resChambre8;
    public chambre resChambre9;
    
   private int resultat = 0;



    // Use this for initialization
    void Start () {

        fin.SetActive(false);
        
    }
	
	// Update is called once per frame
	void Update () {

        int v1 = resChambre1.valueChambre;
        int v2 = resChambre2.valueChambre;
        int v3 = resChambre3.valueChambre;
        int v4 = resChambre4.valueChambre;
        int v5 = resChambre5.valueChambre;
        int v6 = resChambre6.valueChambre;
        int v7 = resChambre7.valueChambre;
        int v8 = resChambre8.valueChambre;
        int v9 = resChambre9.valueChambre;

       // Debug.Log("test v1 =" + v1);

        resultat = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9 ;

      //  Debug.Log("Resultat =" + resultat);

		if (resultat == 7)
        {
            SonCheck.GetComponent<AudioSource>().Play();

            fin.SetActive(true);
            zoneClic.SetActive(false);
            GameManager.instance.pause = false ;

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[1] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);


        }

        if (GameManager.instance.disableClick == true)
        {
            zoneClic.SetActive(false);

        }
        else
        {
            zoneClic.SetActive(true);

        }

    }

 //   private int playerPerf;

}



