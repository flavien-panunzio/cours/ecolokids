﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chambre : MonoBehaviour {

    public GameObject chambreObj;
    public GameObject IntOn;
    public GameObject IntOff;


    // private bool robinetOuvertDis = true ;

    public int valueChambre;
    private float time;
    private float timeLimit;



    // Use this for initialization
    void Start()
    {
        chambreObj.SetActive(false);
        valueChambre = 0;
        time = 0;
        timeLimit = 4f;
        //  Debug.Log("Value = " + valueRobinet);
    }

    // Update is called once per frame
    void Update()
    {
        if (valueChambre == 1)
        {
            time += Time.deltaTime;
            if (time >= timeLimit)
            {
                chambreObj.SetActive(false);
                valueChambre = 0;
                time = 0;
                IntOn.GetComponent<AudioSource>().Play();

            }

            //  Debug.Log("Time = " + time);
        }




    }

    private void OnMouseDown()
    {

        if (valueChambre == 1)
        {
            chambreObj.SetActive(false);
            valueChambre = 0;
            // Debug.Log("Value = " + valueRobinet);
            IntOn.GetComponent<AudioSource>().Play();

        }
        else
        {
            chambreObj.SetActive(true);
            valueChambre = 1;
            IntOff.GetComponent<AudioSource>().Play();

        }





        //  Debug.Log("Value = " + valueRobinet);
    }


}
