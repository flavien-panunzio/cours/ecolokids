﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closeMaison : MonoBehaviour
{


    public GameObject debut;
    public GameObject ZoneGrise;
    public GameObject ZoneClic;
    public GameObject[] ZoneClic2;



    // Use this for initialization
    void Start()
    {
        ZoneClic.SetActive(false);
        ZoneGrise.SetActive(false);
        debut.SetActive(true);

        for (int i = 0; i < ZoneClic2.Length; i++)
        {
            ZoneClic2[i].GetComponent<BoxCollider2D>().enabled = false;
        }
        debut.SetActive(true);

        GameManager.instance.pause = false;
        MusiqueManager.instance.NomMusique = "Jeu";
    }

    // Update is called once per frame
    void Update()
    {





    }

    private void OnMouseDown()
    {

        debut.SetActive(false);
        ZoneClic.SetActive(true);
        ZoneGrise.SetActive(true);
        GameManager.instance.pause = true;
        GameManager.instance.disableClick = false;

        for (int i = 0; i < ZoneClic2.Length; i++)
        {
            ZoneClic2[i].GetComponent<BoxCollider2D>().enabled = true;
        }

    }

}
