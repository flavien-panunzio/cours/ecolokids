﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanquiseWin : MonoBehaviour
{
    public GameObject fin;
    public GameObject[] zoneClic;

    public GameObject etat1;
    public GameObject etat2;
    public GameObject etat3;
    public GameObject etat4;
    public GameObject banquise1;
    public GameObject banquise2;
    public GameObject banquise3;
    public GameObject banquise4;
    public GameObject son1;
    public GameObject son2;
    public GameObject son3;


    public DragClef[] resClef;
    private int resultat = 0;



    void Start()
    {
        fin.SetActive(false);
        etat2.SetActive(false);
        etat3.SetActive(false);
        etat4.SetActive(false);
        son1.SetActive(false);
        son2.SetActive(false);
        son3.SetActive(false);
        banquise2.SetActive(false);
        banquise3.SetActive(false);
        banquise4.SetActive(false);
    }


    void Update()
    {
        resultat = resClef[0].valuePuzzle + resClef[1].valuePuzzle + resClef[2].valuePuzzle;

      //  Debug.Log(resultat);


        if (resultat == 1)
        {
            etat2.SetActive(true);
            banquise1.SetActive(false);
            banquise2.SetActive(true);
            son1.SetActive(true);
        }

        if (resultat == 2)
        {
            etat3.SetActive(true);
            banquise2.SetActive(false);
            banquise3.SetActive(true);
            son2.SetActive(true);

        }



        if (resultat == 3)
        {
            etat4.SetActive(true);
            banquise3.SetActive(false);
            banquise4.SetActive(true);
            son3.SetActive(true);


            fin.SetActive(true);
            //  Debug.Log("Gagne");

            GameManager.instance.pause = false;

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[6] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);

        }

        if (GameManager.instance.disableClick == true)
        {
            for (int i = 0; i < resClef.Length; i++)
            {
                resClef[i].GetComponent<BoxCollider2D>().enabled = false;
            }


        }
        else
        {
            for (int i = 0; i < resClef.Length; i++)
            {
                resClef[i].GetComponent<BoxCollider2D>().enabled = true;
            }
        }

    }

}
