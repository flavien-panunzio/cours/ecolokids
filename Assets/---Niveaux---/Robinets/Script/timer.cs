﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour {


    public float time =  5;
    public GameObject consignes;
    public GameObject fondGris;
    public GameObject timerObj;
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime;

        GetComponent<Text>().text = time.ToString("0");

        if (time <= 0)
        {
            time = 0;
            consignes.SetActive(false);
            fondGris.SetActive(false);
            timerObj.SetActive(false);
        }

	}

 
}
