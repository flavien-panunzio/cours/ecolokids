﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robinet : MonoBehaviour
{

    public GameObject robinetFermeObj;
    public GameObject robinetOuvertObj;

    // private bool robinetOuvertDis = true ;

    public int valueRobinet;
    private float time;
    private float timeLimit;



    // Use this for initialization
    void Start()
    {
        robinetFermeObj.SetActive(false);
        robinetOuvertObj.SetActive(true);
        valueRobinet = 0;
        time = 0;
        timeLimit = 1.5f;
      //  Debug.Log("Value = " + valueRobinet);
    }

    // Update is called once per frame
    void Update()
    {
        if (valueRobinet == 1)
        {
            time += Time.deltaTime;
            if (time >= timeLimit)
            {
                robinetFermeObj.SetActive(false);
                robinetOuvertObj.SetActive(true);
                valueRobinet = 0;
                time = 0;
            }

            //  Debug.Log("Time = " + time);
        }

        


    }

    private void OnMouseDown()
    {

        if (valueRobinet == 1)
        {
            robinetFermeObj.SetActive(false);
            robinetOuvertObj.SetActive(true);
            valueRobinet = 0;
           // Debug.Log("Value = " + valueRobinet);
        }
        else
        {
            robinetFermeObj.SetActive(true);
            robinetOuvertObj.SetActive(false);
            valueRobinet = 1;
        }

       



      //  Debug.Log("Value = " + valueRobinet);
    }


}