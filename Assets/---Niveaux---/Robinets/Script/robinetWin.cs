﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class robinetWin : MonoBehaviour {

    public GameObject fondApres;
    public GameObject fin;
    public GameObject robinetOuvert;
    public GameObject robinetFerme;
    public GameObject zoneClic;
    public GameObject check;

    public Robinet resRobinet1;
    public Robinet resRobinet2;
    public Robinet resRobinet3;
    public Robinet resRobinet4;
    public Robinet resRobinet5;
    
   private int resultat = 0;



    // Use this for initialization
    void Start () {

        
        fondApres.SetActive(false);
        fin.SetActive(false);
        
    }
	
	// Update is called once per frame
	void Update () {

        int v1 = resRobinet1.valueRobinet;
        int v2 = resRobinet2.valueRobinet;
        int v3 = resRobinet3.valueRobinet;
        int v4 = resRobinet4.valueRobinet;
        int v5 = resRobinet5.valueRobinet;

       // Debug.Log("test v1 =" + v1);

        resultat = v1 + v2 + v3 + v4 + v5;

      //  Debug.Log("Resultat =" + resultat);

		if (resultat == 5)
        {
            fondApres.SetActive(true);
            fin.SetActive(true);
            zoneClic.SetActive(false);
            GameManager.instance.pause = false ;

            robinetOuvert.SetActive(false);
            robinetFerme.SetActive(true);

            check.GetComponent<AudioSource>().Play();

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[0] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);

        }

        if (GameManager.instance.disableClick == true)
        {
            zoneClic.SetActive(false);

        }
        else
        {
            zoneClic.SetActive(true);

        }

    }

 //   private int playerPerf;

}



