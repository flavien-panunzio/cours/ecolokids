﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bateau : MonoBehaviour
{

    float PosX;
    private Vector3 Direction;
    public int DirBateau ;

    void Start()
    {
        PosX = transform.position.x;
        Direction = Vector3.right;
        DirBateau = 0;
    }

    private void Update()
    {

        PosX = transform.position.x;

        if (PosX >= 5)
        {
            Direction = Vector3.left;
            transform.localScale = new Vector3(-1, 1, 1);
            DirBateau = 1;

        }

        if (PosX <= -5)
        {
            Direction = Vector3.right;
           transform.localScale = new Vector3(1, 1, 1);
            DirBateau = 0;

        }

        transform.Translate(Direction * Time.deltaTime*2);

    }

    

    
}
