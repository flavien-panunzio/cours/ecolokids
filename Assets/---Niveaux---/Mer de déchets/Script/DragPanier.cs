﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragPanier : MonoBehaviour
{

    public GameObject navire;

    public GameObject canette;
    public GameObject conserve;
    public GameObject biere;
    public GameObject bouteille1;
    public GameObject bouteille2;
    public GameObject bouteille3;
    public GameObject sac;
    public GameObject sac2;
    public GameObject poisson1;
    public GameObject poisson2;
    public GameObject poisson3;
    public GameObject attentionPoisson;
    public GameObject check;
    public GameObject faux;

    public int ValueCanette;
    public int ValueConserve;
    public int ValueBiere;
    public int ValueBouteille1;
    public int ValueBouteille2;
    public int ValueBouteille3;
    public int ValueSac;
    public int ValueSac2;
    private int ValuePoisson1 = 0;
    private int ValuePoisson2;
    private int ValuePoisson3;


    Vector2 _lastPosition;
    Vector3 _initPos;

    int _dragDirection = 0; //0 left, 1 right ou 0 up, 1 down

    int _MouseDown ;

    public LayerMask RaycastTargetLayers;

    public bateau bateau;



    // Start is called before the first frame update
    void Start()
    {
        _initPos = navire.transform.position;
        _initPos.z = 0;
        attentionPoisson.SetActive(false);
    }

    private void Update()
    {
        _initPos = navire.transform.position;

        if (bateau.DirBateau == 0)
        {
            _initPos.x = navire.transform.position.x - 2.7f;
        }

        else if (bateau.DirBateau == 1)
        {
            _initPos.x = navire.transform.position.x + 2.7f;

        }


        if (ValueCanette == 1)
        {
            canette.transform.position = transform.position;
        }
        if (ValueConserve == 1)
        {
            conserve.transform.position = transform.position;
        }
        if (ValueBiere == 1)
        {
            biere.transform.position = transform.position;
        }
        if (ValueBouteille1 == 1)
        {
            bouteille1.transform.position = transform.position;
        }
        if (ValueBouteille2 == 1)
        {
            bouteille2.transform.position = transform.position;
        }
        if (ValueBouteille3 == 1)
        {
            bouteille3.transform.position = transform.position;
        }
        if (ValueSac == 1)
        {
            sac.transform.position = transform.position;
        }
        if (ValueSac2 == 1)
        {
            sac2.transform.position = transform.position;
        }
        if (ValuePoisson1 == 1)
        {
            attentionPoisson.SetActive(true);
            ValuePoisson1 = 0;
        }
        if (ValuePoisson2 == 1)
        {
            attentionPoisson.SetActive(true);
            ValuePoisson2 = 0;
        }
        if (ValuePoisson3 == 1)
        {
            attentionPoisson.SetActive(true);
            ValuePoisson3 = 0;
        }

    }

    private void OnMouseDown()
    {
        _MouseDown = 1 ;

    }

    // Update is called once per frame
    void OnMouseDrag()
    {
        Vector3 convertedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        convertedPos = new Vector3(_initPos.x, convertedPos.y, _initPos.z);
        Vector3 Center = new Vector3(transform.position.x + gameObject.GetComponent<BoxCollider2D>().bounds.size.x / 2, transform.position.y, transform.position.z);

        //ici on lance deux rayon de chaque côté afin de savoir à quel endroit arrêter le drag
        RaycastHit2D hitLeft = Physics2D.Raycast(Center, -Vector2.up, 100f, RaycastTargetLayers);
        RaycastHit2D hitRight = Physics2D.Raycast(Center, Vector2.up, 100f, RaycastTargetLayers);

        //afin de voir les rayons, on affiche une vue debug temporaire (à enlever au final)
        Debug.DrawRay(Center, -Vector2.up * hitLeft.distance, Color.green);
        Debug.DrawRay(Center, Vector2.up * hitRight.distance, Color.red);

        //il faut savoir dans quele direction on drag la pièce
        if (convertedPos.y < _lastPosition.y)
        {
            _dragDirection = 0;//left
        }
        else if (convertedPos.y > _lastPosition.y)
        {
            _dragDirection = 1;//right
        }


        if (hitLeft.collider != null && _dragDirection == 0)
        {
            if (convertedPos.y > hitLeft.point.y + gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2)
            {
                transform.position = convertedPos;
            }
            else
            {
                transform.position = new Vector3(_initPos.x, hitLeft.point.y + gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2, _initPos.z);
            }
        }


        if (hitRight.collider != null && _dragDirection == 1)
        {
            if (convertedPos.y < hitRight.point.y - gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2)
            {
                transform.position = convertedPos;
            }
            else
            {
                transform.position = new Vector3(_initPos.x, hitRight.point.y - gameObject.GetComponent<BoxCollider2D>().bounds.size.y / 2, _initPos.z);
            }
        }

        _lastPosition = gameObject.transform.position;

    }

    private void OnMouseUp()
    {
        _MouseDown = 0;

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (_MouseDown == 1 )
        {
            if (collision.gameObject.name == "canette" )
            {
                ValueCanette = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "conserve" )
            {
                ValueConserve = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "biere")
            {
                ValueBiere = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "bouteille1")
            {
                ValueBouteille1 = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "bouteille2")
            {
                ValueBouteille2 = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "bouteille3")
            {
                ValueBouteille3 = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "sac")
            {
                ValueSac = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "sac2")
            {
                ValueSac2 = 1;
                check.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "poisson1")
            {
                ValuePoisson1 = 1;
                faux.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "poisson2")
            {
                ValuePoisson2 = 1;
                faux.GetComponent<AudioSource>().Play();
            }
            if (collision.gameObject.name == "tortue")
            {
                ValuePoisson3 = 1;
                faux.GetComponent<AudioSource>().Play();
            }

        }
        
        
            
        

    }
}
