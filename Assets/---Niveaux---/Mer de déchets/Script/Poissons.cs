﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poissons : MonoBehaviour
{
    float PosX;

    public int Vitesse = 4;
    public enum sens
    {
        Gauche,
        Droite
    };
    public sens Direction;
    

    void Start()
    {
        PosX = transform.position.x;
        
        
    }

    private void Update()
    {

        if (Direction == sens.Droite)
        {
            if (transform.position.x >= 12)
            {
                transform.position = new Vector3(PosX, transform.position.y, transform.position.z);

            }


            transform.Translate(Vector3.right * Time.deltaTime * Vitesse);
        }

        if (Direction == sens.Gauche)
        {
            if (transform.position.x <= -12)
            {
                transform.position = new Vector3(PosX, transform.position.y, transform.position.z);

            }


            transform.Translate(Vector3.left * Time.deltaTime * Vitesse);
        }

    }
}
