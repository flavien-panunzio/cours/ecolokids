﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingCorde : MonoBehaviour
{

    public GameObject Panier;
    float _distPanier;
    float _distAngledroit;
    float _angleCorde;
    Vector3 _initPos;

    public bateau bateau;


    // Start is called before the first frame update
    void Start()
    {
        _initPos = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

        // ---------  DISTANCE PANIER -----------------

        float _vectX = Panier.transform.position.x - this.transform.position.x;
        float _vectY = Panier.transform.position.y + 1  - this.transform.position.y;

        _distPanier = Mathf.Sqrt((_vectX * _vectX) + (_vectY * _vectY)) ;

        //   Debug.Log(_distPanier);



        // ---------  DISTANCE ANGLE DROIT -----------------

        float _vectXx = this.transform.position.x - this.transform.position.x;
        float _vectYy = Panier.transform.position.y + 1  - this.transform.position.y;

        _distAngledroit = Mathf.Sqrt((_vectXx * _vectXx) + (_vectYy * _vectYy));

        //   Debug.Log(_distPanier);


        // ---------- CALCUL ANGLE -------------------

        _angleCorde = Mathf.Acos(_distAngledroit/_distPanier) ;

       // Debug.Log(_angleCorde);

        var angles = transform.rotation.eulerAngles;

        if (bateau.DirBateau == 0)
        {
            angles.z = -_angleCorde * 40;
        }

        else if (bateau.DirBateau == 1)
        {
            angles.z = _angleCorde * 40;

        }

        
        transform.rotation = Quaternion.Euler(angles);
        //  transform.rotation = rot;


        transform.localScale = new Vector3 (_initPos.x, _distPanier, _initPos.z) ;

    }
}
