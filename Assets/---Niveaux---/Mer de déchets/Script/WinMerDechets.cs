﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinMerDechets : MonoBehaviour
{
 
    public GameObject fin;
    public GameObject zoneClic;
    public GameObject poisson1;
    public GameObject poisson2;
    public GameObject poisson3;
    public GameObject panneau;

    public DragPanier resDechet;
    private int resultat = 0;



    void Start()
    {
        fin.SetActive(false);
        poisson1.SetActive(false);
        poisson2.SetActive(false);
        poisson3.SetActive(false);
    }


    void Update()
    {
        resultat = resDechet.ValueCanette + resDechet.ValueConserve + resDechet.ValueBiere + resDechet.ValueBouteille1 + resDechet.ValueBouteille2 + resDechet.ValueBouteille3 +
            resDechet.ValueSac + resDechet.ValueSac2;


        if (resultat == 2)
        {
            poisson1.SetActive(true);

        }

        if (resultat == 4)
        {
            poisson2.SetActive(true);

        }

        if (resultat == 6)
        {
            poisson3.SetActive(true);

        }
        if (resultat == 8)
        {
            zoneClic.GetComponent<BoxCollider2D>().enabled = false;
            fin.SetActive(true);

            GameManager.instance.pause = false;

            bool[] tableau = new bool[8];
            tableau = PlayerPrefsX.GetBoolArray("FinLvl");
            tableau[7] = true;
            PlayerPrefsX.SetBoolArray("FinLvl", tableau);
            panneau.SetActive(false);

        }

        if (GameManager.instance.disableClick == true)
        {
            zoneClic.GetComponent<BoxCollider2D>().enabled = false;


        }
        else
        {
            zoneClic.GetComponent<BoxCollider2D>().enabled = true;
        } 

    }

}
