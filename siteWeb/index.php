<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="EcoloKids est un jeu développé par l'Agence EcoloKids dans le but d'apprendre les gestes éco-citoyens aux enfants.">
	<meta name="author" content="Agence Hi-Boo">
	<title>EcoloKids</title>
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Mali:400,700" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="TemplateData/style.css" rel="stylesheet">
	<link rel="icon" type="image/png" href="medias/logo.png" />
	<script src="TemplateData/UnityProgress.js"></script>
	<script src="Build/UnityLoader.js"></script>
</head>
<body id="page-top">
	<nav class="navbar navbar-expand-lg navbar-dark bg-blue sticky-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand" href="#page-top">
				<img src="medias/logo.png" alt="logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="#principe">Le Principe</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#jeu">Le Jeu</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#equipe">L'Equipe</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contact">Contact</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="bg-yellow">
		<div class="container text-center">
			<h1>Bienvenue chez EcoloKids</h1>
			<p class="lead">Un jeu pour apprendre les gestes éco-citoyens aux enfants.</p>
			<div class="store">
				<a href="https://play.google.com/store/apps/details?id=com.HiBoo.EcoloKids" target="_blank"><img src="https://cdn.worldvectorlogo.com/logos/google-play-download-android-app.svg"></a>
				<!--<a href="https://www.apple.com/ca/fr/ios/app-store/" target="_blank"><img src="https://qualitedelair.brussels/sites/default/files/app-store-logo.png">--></a>
			</div>
		</div>
	</header>
	<section class="bg-light" id="principe">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					<h2>Le Principe</h2>
					<p class="lead">Le jeu EcoloKids a été développé pour faire découvrir aux enfants les gestes éco-citoyens. Votre enfant pourra apprendre :</p>
					<ul>
						<li>Le tri sélectif des ordures</li>
						<li>Les économies d'eau et d'électricité</li>
						<li>Le ramassage des déchets dans l'océan</li>
						<li>Les énergies renouvelables</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section id="jeu">
		<div class="container">				
			<h2>Le jeu</h2>
			<div class="playGame">
				<img src="medias/logo.png" alt="logo">
				<button class="btn">Jouer au jeu</button>
			</div>
			<div class="webgl-content">
				<div id="gameContainer" style="width: 100%"></div>
				<div class="footer">
					<div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
					<div class="title">Ecolo Kids</div>
				</div>
			</div>
			<p>Si vous avez des difficultées à exécuter le jeu, vous pouvez télécharger le jeu pour <a href="https://play.google.com/store/apps/details?id=com.HiBoo.EcoloKids">Android</a><!-- ou <a href="https://www.apple.com/ca/fr/ios/app-store/">iOS</a>--></p>
		</div>
	</section>
	<section id="equipe" class="bg-light">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					<h2>L'Equipe</h2>
					<div class="row">
						<div class="card cardequipe col-md-5">
							<img src="medias/mathilde.jpg" class="card-img-top" alt="photo de Mathilde CHASSIGNOL">
							<div class="card-body">
								<h5 class="card-title">Mathilde CHASSIGNOL</h5>
								<p class="card-text text-center"><cite>En charge du design des mini-jeux, j'ai aussi revetu  la casquette en tant que monteuse pour la bande annonce.</cite></p>
								<p class="text-center"><a href="https://mmi.univ-smb.fr/~chassigm/Portfolio" target="_blank">Voir le portfolio</a></p>
							</div>
						</div>
						<div class="card cardequipe col-md-5">
							<img src="medias/marion.jpg" class="card-img-top" alt="photo de Marion DUMANET">
							<div class="card-body">
								<h5 class="card-title">Marion DUMANET</h5>
								<p class="card-text text-center"><cite>Je suis graphiste et pour ce projet nous avons créé avec Mathilde les décors et visuelles pour l'application.</cite></p>
								<p class="text-center"><a href="https://www.behance.net/gallery/72863247/portfolio-Marion-Dumanet" target="_blank">Voir le portfolio</a></p>
							</div>
						</div>
						<div class="card cardequipe col-md-5">
							<img src="medias/lucas.jpg" class="card-img-top" alt="photo de Lucas FOURNIER">
							<div class="card-body">
								<h5 class="card-title">Lucas FOURNIER</h5>
								<p class="card-text text-center"><cite>Internet et mon ordi sont mes amis! Je créé et je développe ! Les mini-jeux sont là pour s'amuser ! Il faut toujours une âme d'enfant pour les créer.</cite></p>
								<p class="text-center"><a href="https://lucasf.fr" target="_blank">Voir le portfolio</a></p>
							</div>
						</div>
						<div class="card cardequipe col-md-5">
							<img src="medias/flavien.jpg" class="card-img-top" alt="photo de Flavien PANUNZIO">
							<div class="card-body">
								<h5 class="card-title">Flavien PANUNZIO</h5>
								<p class="card-text text-center"><cite>«Je suis développeur Web et pour ce projet, je me suis occupé de toute l'interface utilisateur sur Unity, une toute nouvelle méthode de travail pour moi.»</cite></p>
								<p class="text-center"><a href="https://www.flavien-panunzio.tk" target="_blank">Voir le portfolio</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					<h2>Contact</h2>
					<form action="envoieMail.php" method="get">
						<table>
							<tr>
								<th><label for="mail">Mail</label></th>
								<td><input class="form-control" type="text" name="mail" id="mail" placeholder="@mail" required /></td>
							</tr>
							<tr>
								<th><label for="Objet">Objet</label></th>
								<td><input class="form-control" type="text" name="objet" id="Objet" placeholder="Objet" required /></td>
							</tr>
							<tr>
								<th><label for="message">Message</label></th>
								<td><textarea class="form-control" name="message" id="message" placeholder="Dis nous tout !"></textarea></td>
							</tr>
							<tr>
								<th></th>
								<td>
									<button class="btn bg-yellow" type="submit">Envoyer</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</section>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Hi-Boo 2019</p>
		</div>
	</footer>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
