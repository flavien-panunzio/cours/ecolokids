$(document).ready(function(){

	$(".playGame button").click(function(event) {
		$(this).parent().addClass('d-none');
		var gameInstance = UnityLoader.instantiate("gameContainer", "Build/Nouveau dossier.json", {onProgress: UnityProgress});
	});


	$('nav a[href^="#"]').click(function(){
		$(this).addClass("active");
		var the_id = $(this).attr("href");
		if (the_id === '#') {
			return;
		}
		$('html, body').animate({
			scrollTop:$(the_id).offset().top-50
		}, 'slow');
		return false;
	});
});