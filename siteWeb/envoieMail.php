<?php 
$headersupplement='<meta http-equiv="refresh" content="2; URL=index.php" />';
$title='Envoie';
include 'header.php' ; ?>

<main>

    <?php

	// Récupération des champs qui sont censés être transmis
	// en fait on récupère les valeurs associés aux noms des champs et 
	// on les stocke dans des variables PHP (commencent par un $).

	// Si les champs avaient été transmis en POST, il aurait fallu écrire $_POST
	// à la place de $_GET.
	
	$destinataire = 'lucas.fournier3@etu.univ-lorraine.fr' ;
	$expediteur = $_GET ['mail'] ;
	$sujet = $_GET ['objet'] ;
	$message = $_GET ['msg'] ;
    $prenom = $_GET ['prenom'] ;
    $nom = $_GET ['nom'] ;
	
	// Ici on ne fait aucun contrôle, on espère que tous les champs sont bien remplis
	// à la soumission du formulaire
	
	// Puis on appelle simplement la fonction mail() prédéfinie en php en lui transmettant 
	// les paramètres qu'elle attend
	
	$headers = 'From: ' . $expediteur ; 
        
  
	$res = mail ( $destinataire , $sujet , $message , $headers ) ;
	
    echo ("Votre mail a bien été envoyé"  ) ;    
        


?>

</main>

<div class="footer-fixed">

    <?php include 'footer.php' ; ?>


</div>
